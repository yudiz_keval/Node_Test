var express = require('express');
var valid = require('validator');
var crypto = require('crypto');
algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq';

var jwt = require('jsonwebtoken');
var router = express.Router()
var parser = require('body-parser')
var key = '123456'

var dbconnect = require('./dbconfig.js')
var smtp = require('./smtpconfig.js')


var urlparser = parser.urlencoded({ extended: true });
var status = { 'ok': 200, 'internalerror': 500, 'Bad request': 400, 'pageNotFound': 404, 'unauthorized': 401, 'Retry': 449 }

let tempString = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
var Token = ''
for (let i = 0; i < 10; i++) {
    Token += tempString.charAt(Math.floor(Math.random() * tempString.length))
}

// ********************* LOGIN FUNCTION **************************************


router.post('/login', urlparser, function (req, res) {

    let postdata = { 'vEmail': req.body.vEmail, 'vPassword': encrypt(req.body.vPassword) }

    if (!valid.isEmpty(postdata.vEmail), !valid.isEmpty(postdata.vPassword) && valid.isEmail(postdata.vEmail)) {
        dbconnect.query('SELECT * FROM register WHERE vEmail = ? AND vpassword = ?', [postdata.vEmail, postdata.vPassword], function (err, row) {
            if (err) throw err;
            if (row.length != 0) {
                let auth = jwt.sign(postdata, Token);
                dbconnect.query('update register set vauth = ? where vEmail = ?', [auth, postdata.vEmail], function (err, row) {
                    if (err) {
                        res.json({ 'message': err })
                    }
                    if (row.affectedRows != 0) {
                        res.json({ 'status code': status.ok, 'message': 'Login successfuly', 'auth': auth })
                    }
                })
            }
            else {
                res.json({ 'status code': status.Retry, 'message': 'Username or Password incorrect' })
            }
        })
    }
    else {
        res.json({ 'status code': status.Retry, 'message': 'please Enter Valid Data' })
    }

})

// ********************* SIGN UP FUNCTION ***************************************** 

router.post('/signup', urlparser, function (req, res) {

    let postdata = { 'vName': req.body.vName, 'vEmail': req.body.vEmail, 'vPassword': encrypt(req.body.vPassword) }

    if (!valid.isEmpty(postdata.vName, postdata.vEmail, postdata.vPassword) && valid.isEmail(postdata.vEmail)) {

        dbconnect.query('select * from register where vEmail = ?', postdata.vEmail, function (err, row) {
            if (err) throw err;
            if (row.length > 0) {
                res.json({ 'message': 'Email Already Exist' })
            } else {
                dbconnect.query('INSERT INTO register SET ?', postdata, function (err, result) {
                    if (err) throw err;
                    if (result.affectedRows > 0) {
                        res.json({ 'status code': status.ok, 'message': 'Data added successfuly' });
                    }
                    else {
                        res.json({ 'status code': status.internalerror })
                    }
                })
            }
        })
    }
    else {
        res.json({ 'message': 'Please Fill valid Data' });
    }

})

//  ************************ FORGOT PASSWORD *********************************

router.post('/forgot_password', urlparser, function (req, res) {
    let postdata = { 'vEmail': req.body.vEmail, 'vToken': Token }
    if (valid.isEmail(postdata.vEmail) && !valid.isEmpty(postdata.vEmail)) {
        dbconnect.query('select vEmail from register where vEmail = ?', postdata.vEmail, function (err, row, field) {
            if (err) throw err;
            if (row.length > 0) {
                dbconnect.query('select vEmail from forgot_password where status = ?', ['n'], function (err, result) {
                    if (err) throw err;
                    if (result.length > 0) {
                        dbconnect.query('UPDATE forgot_password SET vToken = ? where vEmail = ? AND status = ?', [Token, result[0].vEmail, 'n'], function (err, row) {
                            if (err) throw err;
                            if (result.affectedRows != 0) {
                                sendEmail(postdata.vEmail, function (data) {
                                    res.json({ 'message': 'OTP Send to Ur Email' })
                                })
                            }
                        })
                    }
                    else {
                        dbconnect.query('INSERT INTO forgot_password SET ?', postdata, function (err, result) {
                            if (err) throw err;
                            if (result.affectedRows != 0) {
                                sendEmail(postdata.vEmail, function (data) {
                                    res.json({ 'message': 'OTP Send to Ur Email' })
                                })
                            }
                            else {
                                res.json({ 'message': 'Server Internal problem' });
                            }
                        })
                    }
                })

            }
            else {
                res.json({ 'message': 'Email Id Not found' })
            }
        })

    } else {
        res.json({ 'status code': status.Retry, 'message': 'Please Enter ur Email Id' });
    }

})

// *********************** CHANGE PASSWORD *********************************

router.post('/verify', urlparser, function (req, res) {
    let auth = req.headers['authentication'];
    let postdata = { 'vPassword': encrypt(req.body.vPassword), 'vToken': req.body.vToken }
    if (!valid.isEmpty(postdata.vToken, postdata.vEmail, auth)) {
        dbconnect.query('select * from forgot_password join register as R  where vToken = ? AND status = ?  AND R.vauth = ?', [postdata.vToken, 'n', auth], function (err, result) {
            if (err) throw err;
            if (result.length != 0) {
                dbconnect.query('Update register as R join forgot_password as FR set R.vpassword = ?,FR.status = ? WHERE R.vEmail = FR.vEmail', [postdata.vPassword, 'y'], function (err, result) {
                    if (err) throw err;
                    if (result != 0) {
                        res.json({ 'message': 'Ur password is update successfuly' })
                    } else {
                        res.json({ 'message': 'Invalid' })
                    }

                })
            }
            else {
                res.json({ 'message': 'Invalid' })
            }

        })
    } else {
        res.json({ 'status code': status.Retry, 'message': 'Please Enter proper Data' });
    }
})

// *********************** Update Use Profile *********************************

router.post('/update_User_profile', urlparser, function (req, res) {
    let auth = req.headers['authentication']
    let post = { 'vName': req.body.vName, 'vEmail': req.body.vEmail }
    if (!valid.isEmpty(post.vName, post.vEmail) && auth != undefined) {
        dbconnect.query('update register set vEmail = ?,vName = ? where vauth = ?', [post.vEmail, post.vName, auth], function (err, row) {
            if (err) {
                throw err;
                // res.json({ 'message': err })
            } else {
                if (row.affectedRows != 0)
                    res.json({ 'message': 'profile update successfuly' });
                else
                    res.json({ 'message': 'please Enter valid Data' });
            }
        })
    } else {
        res.json({ 'message': 'please Enter valid Data' });
    }

})

// *********************** Retrive All User *********************************

router.post('/get_AllUser', function (req, res) {
    dbconnect.query('select id,vname,vEmail from register', function (err, rows) {
        if (err) {
            res.json({ 'message': 'Server Internal problem', 'error': err });
        }
        else {
            if (rows.affectedRows != 0) {
                res.json({ 'status code': status.ok, 'message': 'data retrive successfuly', 'data': rows })
            }
            else {
                res.json({ 'status code': status.ok, 'message': 'data retrive successfuly', 'data': rows })
            }
        }
    })
})

// *********************** Delete User  *********************************

router.post('/delete_user', urlparser, function (req, res) {
    if (!valid.isEmpty(req.body.id)) {
        dbconnect.query('delete from register where id = ?', [req.body.id], function (err, rows) {
            if (err) {
                res.json({ 'message': 'Server Internal problem', 'error': err });
            }
            else {
                if (rows.affectedRows != 0) {
                    res.json({ 'status code': status.ok, 'message': 'data retrive successfuly', 'data': rows })
                }
                else {
                    res.json({ 'status code': status.ok, 'message': 'No any data' })
                }
            }
        })
    }
    else {
        res.json({'status code': status.ok, 'message': 'please Enter valid Data' });
    }
})


// *********************** SEND EMAIL  *********************************

function sendEmail(Email, callback) {
    let mailOptions = {
        from: 'keval.nayak@ping2world.com',
        to: Email,
        subject: 'ForGot Password',
        text: 'Your OTP is = ' + Token,
    };
    smtp.sendMail(mailOptions, function (error, info) {
        if (error) {
            callback({ 'status code': status.internalerror, 'message': 'Server Internal Error' });
        }
        else
            callback(info);        
    })
}

// *********************** Bad Request *********************************

router.get('*', function (req, res) {
    res.json({ 'status code': status.pageNotFound, 'message': 'Access Denied' });
})

// *********************** Encrypt Password  *********************************
function encrypt(text) {
    var cipher = crypto.createCipher(algorithm, password)
    var crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted;
}

module.exports = router;